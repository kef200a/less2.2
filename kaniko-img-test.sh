#!/usr/bin/bash

docker run -ti --rm \
    -v "$PWD/web":/workspace \
    -v ~/.docker/config.json:/kaniko/.docker/config.json:ro \
    gcr.io/kaniko-project/executor:v0.15.0 \
    --cache \
    --dockerfile=Dockerfile \
    --destination=registry.gitlab.com/kef200a/less1.1-fork/app_dev-kaniko-01
